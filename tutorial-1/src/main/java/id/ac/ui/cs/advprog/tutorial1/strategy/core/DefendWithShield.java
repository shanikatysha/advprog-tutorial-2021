package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    public String getType() {
        return "Protective Shield";
    }

    public String defend() {
        return "assembling... shield ready to use";
    }
    //ToDo: Complete me
}
