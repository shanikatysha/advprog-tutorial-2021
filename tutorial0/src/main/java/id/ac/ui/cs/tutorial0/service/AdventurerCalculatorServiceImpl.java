package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }

    //added
    @Override
    public String countClassCategoryFromPower(int birthYear) {
        int powerRes = countPowerPotensialFromBirthYear(birthYear);
        if (0 <= powerRes && powerRes < 2000) {
            return "C class";
        } else if (2000 <= powerRes && powerRes < 100000) {
            return "B class";
        } else if (powerRes >= 100000) {
            return "A class";
        } else {
            return "unknown";
        }
    }



    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
